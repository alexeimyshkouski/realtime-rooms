const { service } = require('..')

class TestNode {
    static IncomingContext = class Base {
        constructor() {
            console.log('base created')
        }

        send() {
            console.log('sent')
        }
    }
}

const extended = service(TestNode)

const ec = new extended.IncomingContext()

console.log(ec instanceof extended.IncomingContext, ec instanceof TestNode.IncomingContext)
console.log(ec.send())