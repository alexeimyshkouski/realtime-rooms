import path from 'path'

import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import external from 'rollup-external'

const __dist = path.resolve('dist')

const formats = new Map([
    ['cjs', {
        dir: __dist,
        entryFileNames: '[name].js',
        chunkFileNames: 'chunks/[hash].js'
    }],
    ['esm', {
        dir: __dist,
        entryFileNames: '[name].mjs',
        chunkFileNames: 'chunks/[hash].mjs',
        preferConst: true
    }]
].map((entry) => {
    entry[1].format = entry[0]
    return entry
}))

export default {
    input: 'src/index.js',
    output: [...formats.values()],
    external: external(),
    plugins: [
        resolve(),
        babel({
            presets: [
                ['@babel/preset-env', { targets: ['maintained node versions'] }]
            ],
            plugins: [
                ['@babel/plugin-proposal-decorators', { decoratorsBeforeExport: true }],
                '@babel/plugin-proposal-class-properties'
            ]
        })
    ]
}