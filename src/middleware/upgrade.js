export default async (ctx, next) => {
    await next()

    if (!ctx.app.rooms.has(ctx.socket)) {
        ctx.app.rooms.set(ctx.socket, new Map())

        ctx.socket.once('close', function () {
            this.removeAllListeners()

            const rooms = ctx.app.rooms.get(this)

            for (const token of rooms.values()) {
                token.unsubscribe()
            }

            ctx.app.rooms.delete(this)
        })
    }
}