import { serialize, deserialize } from '../utils/message'

export default () => {
    return (ctx, next) => {
        let { message } = ctx

        if (!ctx.rawMessage) {
            ctx.rawMessage = message
        }

        Object.assign(ctx, { serialize, deserialize })

        try {
            const { scope, payload, id } = deserialize(message)

            Object.assign(ctx, { scope, payload, id })
        } catch(error) {
            ctx.send(['!/*', { code: error.code || undefined, message: error.message || undefined }])

            return
        }

        return next()
    }
}
