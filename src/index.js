import Hub from '@alexeimyshkouski/pubsub'

import * as scopes from './scopes'
import RoomsRouter from './router'

import { withRooms } from './context-decorators/with-rooms'
import upgradeMiddleware from './middleware/upgrade'

export { RoomsRouter as Router }

export const service = Node => class extends Node {
    _router = new RoomsRouter()
    hub = new Hub()
    rooms = new Map()

    static IncomingContext = @withRooms class RoomIncomingContext extends Node.IncomingContext {}

    constructor() {
        super()

        this
            .upgrade(upgradeMiddleware)
            .incoming(this._router.middleware())
    }

    enter(room, fn) {
        this._router.message(scopes.enter.token + room, fn)

        return this
    }

    leave(room, fn) {
        this._router.message(scopes.leave.token + room, fn)

        return this
    }

    event(room, fn) {
        this._router.message(scopes.event.token + room, fn)
    }
}