import RealtimeRouter from '@alexeimyshkouski/realtime-router'

import * as scopes from './scopes'
import parseMessage from './middleware/parse-message-json'

const channelParamString = ':channel'

async function onEnter(ctx, next) {
    await next()

    const entered = ctx.enter()
    ctx.respond(entered)
}

async function onLeave(ctx, next) {
    await next()

    const left = ctx.leave()
    ctx.respond(left)
}

async function onEvent(ctx, next) {
    if (ctx.rooms.has(ctx.channel)) {
        await next()

        ctx.room.broadcast(ctx.payload)
    }
}

export default class RoomsRouter extends RealtimeRouter {
    constructor(...args) {
        super(...args)

        this
            .message(parseMessage())
            .message(scopes.enter.token + channelParamString, onEnter)
            .message(scopes.leave.token + channelParamString, onLeave)
            .message(scopes.event.token + channelParamString, onEvent.bind(this))
    }
}