import { createEventMessage } from '../utils/message'

function _emit(connection, serialize, channel, payload) {
    const message = serialize(createEventMessage(channel, payload))

    connection.send(message)
}

const _extend = Context => class extends Context {
    get channel() {
        return this.params && this.params.channel
        // return Hub.normalizeName(this.params && this.params.channel)
    }

    get room() {
        return this.rooms.get(this.channel)
    }

    get rooms() {
        return this.app.rooms.get(this.socket)
    }

    enter(channel = this.channel) {
        const ctx = this

        if (!ctx.rooms.has(channel)) {
            const token = ctx.app.hub.subscribe(channel, _emit.bind(this, ctx.connection, ctx.serialize.bind(undefined), channel))

            if (token) {
                ctx.rooms.set(token.channel, token)

                return true
            }
        }

        return false
    }

    leave(channel = this.channel) {
        const ctx = this

        if (ctx.rooms.has(channel)) {
            const token = ctx.room
            token.unsubscribe()
            ctx.rooms.delete(channel)

            return true
        }

        return false
    }

    respond(payload) {
        const ctx = this
        return ctx.send([ctx.originalScope, payload])
    }

    emit(payload) {
        const ctx = this
        return ctx.send(createEventMessage(ctx.channel, payload))
    }

    send(message) {
        const { id } = this

        if (id !== undefined) {
            if (Array.isArray(message)) {
                message = [...message, id]
            } else if ((typeof message)[0] == 'o') {
                message = Object.assign({}, message, { id })
            }
        }

        return super.send(message)
    }
}

export const withRooms = target => {
    if (target.kind === 'class') {
        target.finisher = value => _extend(value)
    } else if (target.placement === 'static') {
        let value

        if (target.kind === 'field') {
            if (!target.initializer) {
                throw new ReferenceError(`Property "${target.key}" should have an initializer`)
            }

            value = target.initializer()
            value = _extend(value)
            target.initializer = () => value
        } else if (target.kind === 'method') {
            if(!target.descriptor.get) {
                throw new ReferenceError('Property descriptor should have a getter')
            }

            value = target.descriptor.get()
            value = _extend(value)
            target.descriptor.get = () => value
        } else {
            throw new TypeError('Decorator descriptor should have "field" or "method" kind')
        }
    } else {
        throw new TypeError('Decorator descriptor should have "class" kind or "static" placement')
    }

    return target
}