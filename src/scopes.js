export const echo = { token: '#/',  re: '(#|/echo)/' }
export const event = { token: '!/',  re: '(!|/event)/' }
export const enter = { token: '+/',  re: '(\\+|/enter)/' }
export const leave = { token: '-/',  re: '(\\-|/leave)/' }