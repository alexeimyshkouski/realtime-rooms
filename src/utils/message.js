import { event as eventToken } from '../scopes'

export function deserialize(message) {
    const typeOfMessage = typeof message

    if (typeOfMessage !== 'object') {
        if (Buffer.isBuffer(message)) {
            message = message.toString()
        } else if (typeOfMessage !== 'string') {
            throw new TypeError('Cannot parse message of type "' + typeOfMessage + '"')
        }

        message = JSON.parse(message)
    }

    let scope, payload, id

    if (Array.isArray(message)) {
        scope = message[0]
        payload = message[1]

        if (2 in message) {
            id = message[2]
        }
    } else {
        scope = message.scope
        payload = message.payload

        if ('id' in message) {
            id = message.id
        }
    }

    return { scope, payload, id }
}

export function serialize(message) {
    try {
        return JSON.stringify(message)
    } catch (error) {
        throw new TypeError('Cannot stringify message "' + message + '"')
    }
}

export function createEventMessage(channel, payload) {
    return [eventToken.token + channel, payload]
}